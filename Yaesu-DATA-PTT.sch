<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="mini_din">
<packages>
<package name="M_DIN6">
<wire x1="7" y1="4.75" x2="-7" y2="4.75" width="0.127" layer="21"/>
<wire x1="-7" y1="4.75" x2="-7" y2="0.25" width="0.127" layer="22"/>
<wire x1="-7" y1="0.25" x2="-7" y2="-8.05" width="0.127" layer="22"/>
<wire x1="-7" y1="-8.05" x2="7" y2="-8" width="0.127" layer="21"/>
<wire x1="7" y1="-8" x2="7" y2="-1.75" width="0.127" layer="22"/>
<wire x1="7" y1="-1.75" x2="7" y2="0.25" width="0.127" layer="22"/>
<wire x1="7" y1="0.25" x2="7" y2="4.75" width="0.127" layer="22"/>
<wire x1="7" y1="0.25" x2="6.7" y2="0.25" width="0.127" layer="22"/>
<wire x1="6.7" y1="0.25" x2="6.7" y2="-1.75" width="0.127" layer="22"/>
<wire x1="6.7" y1="-1.75" x2="7" y2="-1.75" width="0.127" layer="22"/>
<wire x1="-7" y1="0.25" x2="-6.7" y2="0.25" width="0.127" layer="22"/>
<wire x1="-6.7" y1="0.25" x2="-6.7" y2="-1.75" width="0.127" layer="22"/>
<wire x1="-6.7" y1="-1.75" x2="-6.95" y2="-1.75" width="0.127" layer="22"/>
<pad name="SH1" x="6.8" y="-0.75" drill="2.3" diameter="3.5"/>
<pad name="SH3" x="-6.75" y="-0.75" drill="2.3" diameter="3.5"/>
<pad name="SH2" x="0" y="0.05" drill="2.3" diameter="3.5"/>
<pad name="2" x="1.3" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="1" x="-1.3" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="3" x="-3.4" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="5" x="3.4" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="8" x="3.4" y="-6.25" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="6" x="-3.4" y="-6.2" drill="0.9" diameter="1.4224" shape="octagon"/>
<text x="-2.286" y="-5.03" size="0.8128" layer="21">1</text>
<text x="0.092" y="-5.03" size="0.8128" layer="21">2</text>
<text x="-4.58" y="-5.03" size="0.8128" layer="21">3</text>
<text x="2.28" y="-5.07" size="0.8128" layer="21">5</text>
<text x="-4.656" y="-7.53" size="0.8128" layer="21">6</text>
<text x="2.112" y="-7.57" size="0.8128" layer="21">8</text>
<text x="-6.35" y="5.13" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.59" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="M_DIN6">
<wire x1="-2.667" y1="-2.159" x2="-2.667" y2="-0.889" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="1.905" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="3.175" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="-2.159" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="8.89" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.874" y1="-2.54" x2="-3.302" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.715" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.1524" layer="94"/>
<wire x1="1.27" y1="11.43" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-1.524" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.254" layer="94" curve="-180"/>
<wire x1="-3.81" y1="4.445" x2="-3.81" y2="5.715" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="5.08" x2="-4.445" y2="5.08" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.08" x2="5.715" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="8.89" y2="-6.35" width="0.1524" layer="94"/>
<circle x="0" y="2.54" radius="7.62" width="0.8128" layer="94"/>
<text x="0" y="13.335" size="1.778" layer="96">&gt;VALUE</text>
<text x="-10.16" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.302" y="4.318" size="1.778" layer="94">8</text>
<text x="5.969" y="-5.715" size="1.524" layer="94">PE</text>
<text x="-8.636" y="-5.715" size="1.524" layer="94">PE</text>
<text x="0.889" y="-2.286" size="1.778" layer="94">1</text>
<text x="-2.159" y="-2.286" size="1.778" layer="94">2</text>
<text x="3.175" y="1.778" size="1.778" layer="94">3</text>
<text x="-4.572" y="1.778" size="1.778" layer="94">5</text>
<text x="1.778" y="4.318" size="1.778" layer="94">6</text>
<rectangle x1="-1.27" y1="8.89" x2="1.27" y2="11.43" layer="94"/>
<rectangle x1="-1.27" y1="-5.715" x2="1.27" y2="-4.445" layer="94"/>
<rectangle x1="-1.016" y1="1.778" x2="1.27" y2="5.842" layer="94"/>
<pin name="3" x="15.24" y="2.54" visible="off" direction="pas" rot="R180"/>
<pin name="1" x="15.24" y="-2.54" visible="off" direction="pas" rot="R180"/>
<pin name="5" x="-15.24" y="2.54" visible="off" direction="pas"/>
<pin name="2" x="-15.24" y="-2.54" visible="off" direction="pas"/>
<pin name="PE1" x="12.7" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="6" x="12.7" y="5.08" visible="off" direction="pas" rot="R180"/>
<pin name="8" x="-12.7" y="5.08" visible="off" direction="pas"/>
<pin name="PE3" x="-12.7" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1"/>
<pin name="PE2" x="0" y="-10.16" visible="off" length="middle" direction="pwr" swaplevel="1" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="3.048" y="-4.318"/>
<vertex x="3.048" y="-2.794"/>
<vertex x="5.588" y="-2.794"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="-3.048" y="-4.318"/>
<vertex x="-3.048" y="-2.794"/>
<vertex x="-5.588" y="-2.794"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MINI_DIN_6" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="M_DIN6" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="M_DIN6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="PE1" pad="SH1"/>
<connect gate="G$1" pin="PE2" pad="SH2"/>
<connect gate="G$1" pin="PE3" pad="SH3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DIGIBOX">
<packages>
<package name="ETAL-P5122">
<pad name="1" x="-5.08" y="-2.54" drill="1"/>
<pad name="2" x="2.54" y="-2.54" drill="1"/>
<pad name="4" x="-5.08" y="10.16" drill="1"/>
<pad name="3" x="2.54" y="10.16" drill="1"/>
<wire x1="-5.08" y1="8.89" x2="-5.08" y2="6.35" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="8.89" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21" curve="-180"/>
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.127" layer="21" curve="-180"/>
<wire x1="0" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21" curve="-180"/>
<wire x1="-5.08" y1="6.35" x2="-2.54" y2="6.35" width="0.127" layer="21" curve="180"/>
<wire x1="-2.54" y1="6.35" x2="0" y2="6.35" width="0.127" layer="21" curve="180"/>
<wire x1="0" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21" curve="180"/>
<text x="-6.35" y="0" size="1.27" layer="21" rot="R90">600Ohm 1:1</text>
<text x="5.08" y="0" size="1.27" layer="21" rot="R90">ETAL-P5122</text>
<wire x1="-10.16" y1="12.7" x2="7.62" y2="12.7" width="0.127" layer="21"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-5.08" width="0.127" layer="21"/>
<wire x1="7.62" y1="-5.08" x2="-10.16" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="12.7" width="0.127" layer="21"/>
<text x="-10.16" y="13.97" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.16" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ETAL-P5122">
<pin name="1" x="-5.08" y="-7.62" length="short" rot="R90"/>
<pin name="2" x="2.54" y="-7.62" length="short" rot="R90"/>
<pin name="3" x="2.54" y="7.62" length="short" rot="R270"/>
<pin name="4" x="-5.08" y="7.62" length="short" rot="R270"/>
<wire x1="-7.62" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-8.89" y="-5.08" size="1.27" layer="94" rot="R90">&gt;NAME</text>
<text x="7.62" y="-5.08" size="1.27" layer="94" rot="R90">&gt;VALUE</text>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="1.778" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.334" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="0" y2="1.778" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="1.778" x2="-2.54" y2="1.778" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="1.778" x2="-5.08" y2="1.778" width="0.254" layer="94" curve="-194.250033"/>
<wire x1="-5.08" y1="-1.27" x2="-2.54" y2="-1.524" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="-1.524" x2="0" y2="-1.524" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="-1.524" x2="2.54" y2="-1.27" width="0.254" layer="94" curve="-180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ETAL-P5122">
<gates>
<gate name="G$1" symbol="ETAL-P5122" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="ETAL-P5122">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.413" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND2" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND2" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="GND2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="optocoupler">
<description>&lt;b&gt;Opto Couplers&lt;/b&gt;&lt;p&gt;
Siemens, Hewlett-Packard, Texas Instuments, Sharp, Motorola&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL06">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="3.81" y1="2.921" x2="-3.81" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.921" x2="3.81" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.921" x2="3.81" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="2.921" x2="-3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.921" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.016" x2="-3.81" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-2.54" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="2" x="0" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="5" x="0" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<pad name="6" x="-2.54" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<pad name="3" x="2.54" y="-3.81" drill="0.8128" shape="offset" rot="R270"/>
<pad name="4" x="2.54" y="3.81" drill="0.8128" shape="offset" rot="R90"/>
<text x="-2.413" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.064" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="OK-B">
<wire x1="0" y1="-2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="-2.413" x2="-1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="-1.905" y2="-1.397" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.397" x2="-1.397" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.397" y1="-1.905" x2="-1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0.127" x2="-2.032" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.254" x2="-1.524" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-0.762" x2="-1.143" y2="0.127" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-1.143" y2="0.127" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="0" x2="-4.445" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-5.715" y2="0" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-5.715" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-5.715" y2="0" width="0.254" layer="94"/>
<wire x1="-6.985" y1="-7.62" x2="4.445" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="4.445" y1="5.08" x2="4.445" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-4.445" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-4.445" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-5.08" x2="-7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="4.445" y2="5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="0" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="3.556" y2="-4.826" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-3.556" x2="3.556" y2="-4.826" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-4.826" x2="3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-4.826" x2="2.286" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-4.318" x2="3.048" y2="-3.556" width="0.1524" layer="94"/>
<text x="-6.985" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.985" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="0.889" y1="-5.08" x2="1.651" y2="0" layer="94"/>
<pin name="A" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="BAS" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="EMI" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="COL" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CNY17" prefix="OK">
<description>&lt;b&gt;SIEMENS OPTO COUPLER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="OK-B" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="DIL06">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="BAS" pad="6"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="COL" pad="5"/>
<connect gate="G$1" pin="EMI" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="CNY17-1" constant="no"/>
<attribute name="OC_FARNELL" value="1045399" constant="no"/>
<attribute name="OC_NEWARK" value="05M4044" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA05-1" urn="urn:adsk.eagle:footprint:8283/1" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.35" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="4.445" y="1.651" size="1.27" layer="21" ratio="10">5</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA05-1" urn="urn:adsk.eagle:package:8332/1" type="box" library_version="2">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA05-1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MA05-1" urn="urn:adsk.eagle:symbol:8282/1" library_version="2">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA05-1" urn="urn:adsk.eagle:component:8379/2" prefix="SV" uservalue="yes" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8332/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="29" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="MINI-DIN-6P" library="mini_din" deviceset="MINI_DIN_6" device=""/>
<part name="LEFT_AUDIO" library="DIGIBOX" deviceset="ETAL-P5122" device=""/>
<part name="MIC_IN" library="DIGIBOX" deviceset="ETAL-P5122" device=""/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND2" device=""/>
<part name="OK1" library="optocoupler" deviceset="CNY17" device=""/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="SV1" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA05-1" device="" package3d_urn="urn:adsk.eagle:package:8332/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="MINI-DIN-6P" gate="G$1" x="50.8" y="96.52" smashed="yes">
<attribute name="VALUE" x="50.8" y="109.855" size="1.778" layer="96"/>
<attribute name="NAME" x="40.64" y="109.855" size="1.778" layer="95"/>
</instance>
<instance part="LEFT_AUDIO" gate="G$1" x="88.9" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="93.98" y="82.55" size="1.27" layer="94" rot="R180"/>
<attribute name="VALUE" x="93.98" y="99.06" size="1.27" layer="94" rot="R180"/>
</instance>
<instance part="MIC_IN" gate="G$1" x="88.9" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="93.98" y="102.87" size="1.27" layer="94" rot="R180"/>
<attribute name="VALUE" x="93.98" y="119.38" size="1.27" layer="94" rot="R180"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="73.66" y="81.28" smashed="yes">
<attribute name="VALUE" x="71.755" y="78.105" size="1.016" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="101.6" y="81.28" smashed="yes">
<attribute name="VALUE" x="99.187" y="78.105" size="1.016" layer="96"/>
</instance>
<instance part="OK1" gate="G$1" x="88.9" y="68.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="95.885" y="74.295" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="95.885" y="58.42" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="50.8" y="60.96" smashed="yes">
<attribute name="VALUE" x="48.895" y="57.785" size="1.016" layer="96"/>
</instance>
<instance part="SV1" gate="G$1" x="124.46" y="99.06" smashed="yes" rot="R180">
<attribute name="VALUE" x="125.73" y="109.22" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="125.73" y="90.678" size="1.778" layer="95" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="TXR_OUT" class="0">
<segment>
<pinref part="MINI-DIN-6P" gate="G$1" pin="6"/>
<pinref part="MIC_IN" gate="G$1" pin="3"/>
<wire x1="63.5" y1="101.6" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="81.28" y2="114.3" width="0.1524" layer="91"/>
<label x="74.422" y="114.554" size="1.016" layer="95"/>
</segment>
</net>
<net name="TXR_IN" class="0">
<segment>
<pinref part="MINI-DIN-6P" gate="G$1" pin="1"/>
<pinref part="LEFT_AUDIO" gate="G$1" pin="3"/>
<wire x1="66.04" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
<label x="75.692" y="94.234" size="1.016" layer="95"/>
</segment>
</net>
<net name="GND2" class="0">
<segment>
<pinref part="LEFT_AUDIO" gate="G$1" pin="1"/>
<pinref part="SUPPLY2" gate="G$1" pin="GND2"/>
<wire x1="96.52" y1="86.36" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="101.6" y1="86.36" x2="101.6" y2="83.82" width="0.1524" layer="91"/>
<pinref part="MIC_IN" gate="G$1" pin="1"/>
<wire x1="96.52" y1="106.68" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<wire x1="101.6" y1="106.68" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<junction x="101.6" y="86.36"/>
<wire x1="101.6" y1="99.06" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="116.84" y1="99.06" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<junction x="101.6" y="99.06"/>
<pinref part="SV1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="AUDIO_OUT" class="0">
<segment>
<pinref part="LEFT_AUDIO" gate="G$1" pin="2"/>
<wire x1="96.52" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="93.98" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="101.6" x2="116.84" y2="101.6" width="0.1524" layer="91"/>
<label x="104.14" y="101.854" size="1.016" layer="95"/>
<pinref part="SV1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="MIC_IN" class="0">
<segment>
<pinref part="MIC_IN" gate="G$1" pin="2"/>
<wire x1="96.52" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<wire x1="104.14" y1="114.3" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="104.14" y1="104.14" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
<label x="104.648" y="104.394" size="1.016" layer="95"/>
<pinref part="SV1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="MIC_IN" gate="G$1" pin="4"/>
<wire x1="81.28" y1="106.68" x2="73.66" y2="106.68" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="73.66" y1="106.68" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="LEFT_AUDIO" gate="G$1" pin="4"/>
<wire x1="73.66" y1="86.36" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="86.36" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<junction x="73.66" y="86.36"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<pinref part="OK1" gate="G$1" pin="EMI"/>
<wire x1="50.8" y1="63.5" x2="81.28" y2="63.5" width="0.1524" layer="91"/>
<label x="77.724" y="63.754" size="1.016" layer="95"/>
<pinref part="MINI-DIN-6P" gate="G$1" pin="2"/>
<wire x1="35.56" y1="93.98" x2="35.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="35.56" y1="63.5" x2="50.8" y2="63.5" width="0.1524" layer="91"/>
<junction x="50.8" y="63.5"/>
</segment>
</net>
<net name="PTT_TXR" class="0">
<segment>
<pinref part="MINI-DIN-6P" gate="G$1" pin="3"/>
<wire x1="66.04" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<pinref part="OK1" gate="G$1" pin="COL"/>
<wire x1="68.58" y1="99.06" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<wire x1="68.58" y1="68.58" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<label x="74.93" y="68.834" size="1.016" layer="95"/>
</segment>
</net>
<net name="PTT+" class="0">
<segment>
<pinref part="OK1" gate="G$1" pin="A"/>
<wire x1="106.68" y1="93.98" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<wire x1="106.68" y1="71.12" x2="99.06" y2="71.12" width="0.1524" layer="91"/>
<label x="99.06" y="71.628" size="1.016" layer="95"/>
<pinref part="SV1" gate="G$1" pin="5"/>
<wire x1="106.68" y1="93.98" x2="116.84" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PTT-" class="0">
<segment>
<pinref part="OK1" gate="G$1" pin="C"/>
<wire x1="109.22" y1="63.5" x2="99.06" y2="63.5" width="0.1524" layer="91"/>
<wire x1="109.22" y1="96.52" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
<label x="99.06" y="63.754" size="1.016" layer="95"/>
<pinref part="SV1" gate="G$1" pin="4"/>
<wire x1="109.22" y1="96.52" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
