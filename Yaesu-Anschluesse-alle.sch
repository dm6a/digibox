<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA04-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<text x="-4.318" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="2.921" size="1.27" layer="21" ratio="10">8</text>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA04-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA04-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA04-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="mini_din">
<packages>
<package name="M_DIN8">
<wire x1="7" y1="4.7" x2="-7" y2="4.7" width="0.127" layer="21"/>
<wire x1="-7" y1="4.7" x2="-7" y2="0.2" width="0.127" layer="21"/>
<wire x1="-7" y1="0.2" x2="-7" y2="-8.1" width="0.127" layer="21"/>
<wire x1="-7" y1="-8.1" x2="7" y2="-8.1008" width="0.127" layer="21"/>
<wire x1="7" y1="-8.1008" x2="7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="7" y1="-1.8" x2="7" y2="0.2" width="0.127" layer="21"/>
<wire x1="7" y1="0.2" x2="7" y2="4.7" width="0.127" layer="21"/>
<wire x1="7" y1="0.2" x2="6.7" y2="0.2" width="0.127" layer="21"/>
<wire x1="6.7" y1="0.2" x2="6.7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="6.7" y1="-1.8" x2="7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-7" y1="0.2" x2="-6.7" y2="0.2" width="0.127" layer="21"/>
<wire x1="-6.7" y1="0.2" x2="-6.7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-6.7" y1="-1.8" x2="-6.95" y2="-1.8" width="0.127" layer="21"/>
<pad name="SH1" x="6.8" y="-0.8" drill="2.3" diameter="3.5"/>
<pad name="SH3" x="-6.75" y="-0.8" drill="2.3" diameter="3.5"/>
<pad name="SH2" x="0" y="0" drill="2.3" diameter="3.5"/>
<pad name="2" x="1.3" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="1" x="-1.3" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="3" x="-3.4" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="5" x="3.4" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="8" x="3.4" y="-6.3" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="7" x="0.9" y="-6.3" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="4" x="-1.3" y="-6.3" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="6" x="-3.4" y="-6.25" drill="0.9" diameter="1.4224" shape="octagon"/>
<text x="-2.286" y="-5.08" size="0.8128" layer="21">1</text>
<text x="0.092" y="-5.08" size="0.8128" layer="21">2</text>
<text x="-4.58" y="-5.08" size="0.8128" layer="21">3</text>
<text x="-2.498" y="-7.62" size="0.8128" layer="21">4</text>
<text x="2.28" y="-5.12" size="0.8128" layer="21">5</text>
<text x="-4.656" y="-7.58" size="0.8128" layer="21">6</text>
<text x="-0.28" y="-7.62" size="0.8128" layer="21">7</text>
<text x="2.112" y="-7.62" size="0.8128" layer="21">8</text>
<text x="-6.35" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="M_DIN6">
<wire x1="7" y1="4.75" x2="-7" y2="4.75" width="0.127" layer="21"/>
<wire x1="-7" y1="4.75" x2="-7" y2="0.25" width="0.127" layer="21"/>
<wire x1="-7" y1="0.25" x2="-7" y2="-8.05" width="0.127" layer="21"/>
<wire x1="-7" y1="-8.05" x2="7" y2="-8.0508" width="0.127" layer="21"/>
<wire x1="7" y1="-8.0508" x2="7" y2="-1.75" width="0.127" layer="21"/>
<wire x1="7" y1="-1.75" x2="7" y2="4.75" width="0.127" layer="21"/>
<wire x1="7" y1="0.25" x2="6.7" y2="0.25" width="0.127" layer="22"/>
<wire x1="6.7" y1="0.25" x2="6.7" y2="-1.75" width="0.127" layer="21"/>
<wire x1="6.7" y1="-1.75" x2="7" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-7" y1="0.25" x2="-6.7" y2="0.25" width="0.127" layer="21"/>
<wire x1="-6.7" y1="0.25" x2="-6.7" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-6.7" y1="-1.75" x2="-6.95" y2="-1.75" width="0.127" layer="21"/>
<pad name="SH1" x="6.8" y="-0.75" drill="2.3" diameter="3.5"/>
<pad name="SH3" x="-6.75" y="-0.75" drill="2.3" diameter="3.5"/>
<pad name="SH2" x="0" y="0.05" drill="2.3" diameter="3.5"/>
<pad name="2" x="1.3" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="1" x="-1.3" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="3" x="-3.4" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="5" x="3.4" y="-3.75" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="8" x="3.4" y="-6.25" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="6" x="-3.4" y="-6.2" drill="0.9" diameter="1.4224" shape="octagon"/>
<text x="-2.286" y="-5.03" size="0.8128" layer="21">1</text>
<text x="0.092" y="-5.03" size="0.8128" layer="21">2</text>
<text x="-4.58" y="-5.03" size="0.8128" layer="21">3</text>
<text x="2.28" y="-5.07" size="0.8128" layer="21">5</text>
<text x="-4.656" y="-7.53" size="0.8128" layer="21">6</text>
<text x="2.112" y="-7.57" size="0.8128" layer="21">8</text>
<text x="-6.35" y="5.13" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.59" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="M_DIN8">
<wire x1="-2.667" y1="-4.699" x2="-2.667" y2="-3.429" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="-3.429" x2="2.54" y2="-4.699" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.635" y1="0.381" x2="0.635" y2="0.381" width="0.254" layer="94" curve="180"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.35" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="0" x2="-5.715" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.874" y1="-5.08" x2="-3.302" y2="-4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.524" x2="5.08" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.524" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="8.89" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="6.35" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-4.064" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.254" layer="94" curve="-180"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="3.175" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.635" y1="2.794" x2="0.635" y2="2.794" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="3.429" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-8.89" x2="0" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="0" y1="-8.89" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="-8.89" x2="8.89" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.508" x2="0" y2="-1.524" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="7.62" width="0.8128" layer="94"/>
<text x="0" y="10.795" size="1.778" layer="96">&gt;VALUE</text>
<text x="-10.16" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-1.651" size="1.778" layer="94">4</text>
<text x="-3.302" y="1.778" size="1.778" layer="94">8</text>
<text x="5.969" y="-8.255" size="1.524" layer="94">PE</text>
<text x="-8.636" y="-8.255" size="1.524" layer="94">PE</text>
<text x="0.889" y="-4.826" size="1.778" layer="94">1</text>
<text x="-2.159" y="-4.826" size="1.778" layer="94">2</text>
<text x="3.175" y="-0.762" size="1.778" layer="94">3</text>
<text x="-4.572" y="-0.762" size="1.778" layer="94">5</text>
<text x="1.778" y="1.778" size="1.778" layer="94">6</text>
<text x="0.381" y="3.556" size="1.778" layer="94">7</text>
<rectangle x1="-1.27" y1="6.35" x2="1.27" y2="8.89" layer="94"/>
<rectangle x1="-1.27" y1="-8.255" x2="1.27" y2="-6.985" layer="94"/>
<pin name="3" x="15.24" y="0" visible="off" direction="pas" rot="R180"/>
<pin name="1" x="15.24" y="-5.08" visible="off" direction="pas" rot="R180"/>
<pin name="5" x="-15.24" y="0" visible="off" direction="pas"/>
<pin name="4" x="12.7" y="-2.54" visible="off" direction="pas" rot="R180"/>
<pin name="2" x="-15.24" y="-5.08" visible="off" direction="pas"/>
<pin name="PE1" x="12.7" y="-7.62" visible="off" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="6" x="12.7" y="2.54" visible="off" direction="pas" rot="R180"/>
<pin name="8" x="-12.7" y="2.54" visible="off" direction="pas"/>
<pin name="7" x="-15.24" y="5.08" visible="off" direction="pas"/>
<pin name="PE3" x="-12.7" y="-7.62" visible="off" length="short" direction="pwr" swaplevel="1"/>
<pin name="PE2" x="0" y="-12.7" visible="off" length="middle" direction="pwr" swaplevel="1" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="3.048" y="-6.858"/>
<vertex x="3.048" y="-5.334"/>
<vertex x="5.588" y="-5.334"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="-3.048" y="-6.858"/>
<vertex x="-3.048" y="-5.334"/>
<vertex x="-5.588" y="-5.334"/>
</polygon>
</symbol>
<symbol name="M_DIN6">
<wire x1="-2.667" y1="-2.159" x2="-2.667" y2="-0.889" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="1.905" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="3.175" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="-2.159" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="8.89" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.874" y1="-2.54" x2="-3.302" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.715" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="11.43" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="11.43" x2="1.27" y2="11.43" width="0.1524" layer="94"/>
<wire x1="1.27" y1="11.43" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-1.524" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="5.715" x2="3.81" y2="4.445" width="0.254" layer="94" curve="-180"/>
<wire x1="-3.81" y1="4.445" x2="-3.81" y2="5.715" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="5.08" x2="-4.445" y2="5.08" width="0.1524" layer="94"/>
<wire x1="4.445" y1="5.08" x2="5.715" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-6.35" x2="0" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-6.35" x2="8.89" y2="-6.35" width="0.1524" layer="94"/>
<circle x="0" y="2.54" radius="7.62" width="0.8128" layer="94"/>
<text x="0" y="13.335" size="1.778" layer="96">&gt;VALUE</text>
<text x="-10.16" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.302" y="4.318" size="1.778" layer="94">8</text>
<text x="5.969" y="-5.715" size="1.524" layer="94">PE</text>
<text x="-8.636" y="-5.715" size="1.524" layer="94">PE</text>
<text x="0.889" y="-2.286" size="1.778" layer="94">1</text>
<text x="-2.159" y="-2.286" size="1.778" layer="94">2</text>
<text x="3.175" y="1.778" size="1.778" layer="94">3</text>
<text x="-4.572" y="1.778" size="1.778" layer="94">5</text>
<text x="1.778" y="4.318" size="1.778" layer="94">6</text>
<rectangle x1="-1.27" y1="8.89" x2="1.27" y2="11.43" layer="94"/>
<rectangle x1="-1.27" y1="-5.715" x2="1.27" y2="-4.445" layer="94"/>
<rectangle x1="-1.016" y1="1.778" x2="1.27" y2="5.842" layer="94"/>
<pin name="3" x="15.24" y="2.54" visible="off" direction="pas" rot="R180"/>
<pin name="1" x="15.24" y="-2.54" visible="off" direction="pas" rot="R180"/>
<pin name="5" x="-15.24" y="2.54" visible="off" direction="pas"/>
<pin name="2" x="-15.24" y="-2.54" visible="off" direction="pas"/>
<pin name="PE1" x="12.7" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="6" x="12.7" y="5.08" visible="off" direction="pas" rot="R180"/>
<pin name="8" x="-12.7" y="5.08" visible="off" direction="pas"/>
<pin name="PE3" x="-12.7" y="-5.08" visible="off" length="short" direction="pwr" swaplevel="1"/>
<pin name="PE2" x="0" y="-10.16" visible="off" length="middle" direction="pwr" swaplevel="1" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="3.048" y="-4.318"/>
<vertex x="3.048" y="-2.794"/>
<vertex x="5.588" y="-2.794"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="-3.048" y="-4.318"/>
<vertex x="-3.048" y="-2.794"/>
<vertex x="-5.588" y="-2.794"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MINI_DIN_8" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="M_DIN8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="M_DIN8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="PE1" pad="SH1"/>
<connect gate="G$1" pin="PE2" pad="SH2"/>
<connect gate="G$1" pin="PE3" pad="SH3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINI_DIN_6" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="M_DIN6" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="M_DIN6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="PE1" pad="SH1"/>
<connect gate="G$1" pin="PE2" pad="SH2"/>
<connect gate="G$1" pin="PE3" pad="SH3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DIGIBOX">
<packages>
<package name="SCHRAUBKLEMME-PCB-12M4">
<pad name="3" x="-2.5" y="5" drill="1.5" diameter="3" shape="octagon" rot="R90"/>
<pad name="1" x="-2.5" y="0.5" drill="1.5" diameter="3.048" shape="octagon" rot="R270"/>
<pad name="4" x="5" y="5" drill="1.5" diameter="3.048" shape="octagon" rot="R90"/>
<pad name="2" x="5" y="0.5" drill="1.5" diameter="3.048" shape="octagon" rot="R270"/>
<text x="-2.7554" y="7.5174" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5332" y="-6.8" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.6444" y="2.222" size="1.27" layer="21">KY(M4)</text>
<text x="0.0508" y="-4.41325" size="1.27" layer="21">M4</text>
<wire x1="-2.75" y1="6.881" x2="5.25" y2="6.881" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-4.619" x2="5.25" y2="-4.619" width="0.127" layer="21"/>
<wire x1="5.27685" y1="6.86435" x2="5.27685" y2="-4.61645" width="0.127" layer="21"/>
<wire x1="-2.74955" y1="-4.61645" x2="-2.74955" y2="6.87705" width="0.127" layer="21"/>
<wire x1="-0.75565" y1="-4.6228" x2="-0.75565" y2="-2.6228" width="0.127" layer="21"/>
<wire x1="-0.75565" y1="-2.6228" x2="3.24435" y2="-2.6228" width="0.127" layer="21"/>
<wire x1="3.24435" y1="-2.6228" x2="3.24435" y2="-4.6228" width="0.127" layer="21"/>
<wire x1="3.24435" y1="-4.6228" x2="-0.75565" y2="-4.6228" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-2.921" x2="3.175" y2="-2.921" width="0.127" layer="21"/>
<wire x1="-0.7366" y1="-3.3274" x2="-0.1016" y2="-3.3274" width="0.127" layer="21"/>
<wire x1="-0.7366" y1="-3.683" x2="-0.3556" y2="-3.683" width="0.127" layer="21"/>
<wire x1="-0.7366" y1="-4.064" x2="-0.4826" y2="-4.064" width="0.127" layer="21"/>
<wire x1="-0.7366" y1="-4.445" x2="-0.6096" y2="-4.445" width="0.127" layer="21"/>
<wire x1="3.175" y1="-3.302" x2="2.667" y2="-3.302" width="0.127" layer="21"/>
<wire x1="3.175" y1="-3.683" x2="2.921" y2="-3.683" width="0.127" layer="21"/>
<wire x1="3.175" y1="-4.064" x2="3.048" y2="-4.064" width="0.127" layer="21"/>
<wire x1="3.175" y1="-4.445" x2="3.048" y2="-4.445" width="0.127" layer="21"/>
</package>
<package name="RJ45-MEBP8-8G">
<pad name="1" x="-3.81" y="0" drill="0.9" diameter="1.4224"/>
<pad name="3" x="-1.27" y="0" drill="0.9" diameter="1.4224"/>
<pad name="5" x="1.27" y="0" drill="0.9" diameter="1.4224"/>
<pad name="7" x="3.81" y="0" drill="0.9" diameter="1.4224"/>
<pad name="2" x="-2.54" y="-2.54" drill="0.9" diameter="1.4224"/>
<pad name="4" x="0" y="-2.54" drill="0.9" diameter="1.4224"/>
<pad name="6" x="2.54" y="-2.54" drill="0.9" diameter="1.4224"/>
<pad name="8" x="5.08" y="-2.54" drill="0.9" diameter="1.4224"/>
<hole x="6.35" y="-8.89" drill="3.25"/>
<hole x="-5.08" y="-8.89" drill="3.25"/>
<pad name="G1" x="-7.112" y="-5.842" drill="1.6" diameter="1.778"/>
<pad name="G2" x="8.382" y="-5.842" drill="1.6" diameter="1.778"/>
<text x="-5.08" y="-1.27" size="0.8128" layer="21">1</text>
<text x="-3.81" y="-3.81" size="0.8128" layer="21">2</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="21">3</text>
<text x="-1.27" y="-3.81" size="0.8128" layer="21">4</text>
<text x="0" y="-1.27" size="0.8128" layer="21">5</text>
<text x="1.27" y="-3.81" size="0.8128" layer="21">6</text>
<text x="2.54" y="-1.27" size="0.8128" layer="21">7</text>
<text x="3.81" y="-3.81" size="0.8128" layer="21">8</text>
<wire x1="8.636" y1="1.778" x2="8.636" y2="-19.685" width="0.127" layer="21"/>
<wire x1="8.636" y1="-19.685" x2="-7.239" y2="-19.685" width="0.127" layer="21"/>
<wire x1="-7.239" y1="-19.685" x2="-7.239" y2="1.778" width="0.127" layer="21"/>
<wire x1="-7.239" y1="1.778" x2="8.636" y2="1.778" width="0.127" layer="21"/>
<text x="-2.032" y="-9.652" size="1.6764" layer="21">RJ45</text>
<text x="-4.445" y="-14.986" size="1.6764" layer="21">MEBP 8/8</text>
<text x="-7.62" y="2.54" size="1.6764" layer="21">&gt;NAME</text>
<text x="-7.62" y="-21.844" size="1.6764" layer="21">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SCHRAUBKLEMME-PCB-12M4">
<description>Befestigung für Platienen oder als Schraubklemme nutzba</description>
<pin name="2" x="-2.54" y="5.08" length="short" rot="R270"/>
<pin name="4" x="5.08" y="5.08" length="short" rot="R270"/>
<pin name="3" x="5.08" y="-7.62" length="short" rot="R90"/>
<pin name="1" x="-2.54" y="-7.62" length="short" rot="R90"/>
<wire x1="-5.08" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="-12.192" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.286" y2="-10.16" width="0.254" layer="94"/>
<text x="0.254" y="-11.938" size="1.27" layer="94">M4</text>
<text x="-5.08" y="8.382" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-14.732" size="1.27" layer="96">&gt;VALUE</text>
<text x="2.032" y="-6.096" size="1.27" layer="94" rot="R90">PCB12(M4)</text>
<wire x1="-2.286" y1="-10.16" x2="4.826" y2="-10.16" width="0.254" layer="94"/>
<wire x1="4.826" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.192" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="-10.668" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-10.668" x2="-2.54" y2="-10.668" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.668" x2="-2.54" y2="-11.43" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-11.43" x2="-2.54" y2="-11.43" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-11.43" x2="-2.54" y2="-12.192" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-12.192" x2="-2.286" y2="-12.192" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="-10.668" width="0.254" layer="94"/>
<wire x1="4.826" y1="-10.668" x2="5.08" y2="-10.668" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.668" x2="5.08" y2="-11.43" width="0.254" layer="94"/>
<wire x1="4.826" y1="-11.43" x2="5.08" y2="-11.43" width="0.254" layer="94"/>
<wire x1="5.08" y1="-11.43" x2="5.08" y2="-12.192" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.192" x2="4.826" y2="-12.192" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-10.16" x2="-2.286" y2="-9.652" width="0.254" layer="94"/>
<wire x1="4.826" y1="-10.16" x2="4.826" y2="-9.652" width="0.254" layer="94"/>
</symbol>
<symbol name="RJ45-MEBP8-8G">
<pin name="1" x="-12.7" y="0" length="middle" rot="R90"/>
<pin name="2" x="-10.16" y="0" length="middle" rot="R90"/>
<pin name="3" x="-7.62" y="0" length="middle" rot="R90"/>
<pin name="4" x="-5.08" y="0" length="middle" rot="R90"/>
<pin name="5" x="-2.54" y="0" length="middle" rot="R90"/>
<pin name="6" x="0" y="0" length="middle" rot="R90"/>
<pin name="7" x="2.54" y="0" length="middle" rot="R90"/>
<pin name="8" x="5.08" y="0" length="middle" rot="R90"/>
<wire x1="-15.24" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="5.588" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.588" x2="7.62" y2="2.794" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.794" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-12.7" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-2.54" x2="-15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="2.794" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.794" x2="-15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.858" y1="-5.08" x2="-6.858" y2="-6.858" width="0.254" layer="94"/>
<wire x1="-6.858" y1="-6.858" x2="-0.762" y2="-6.858" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-6.858" x2="-0.762" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-15.24" y1="5.588" x2="-14.478" y2="5.588" width="0.254" layer="94"/>
<wire x1="-14.478" y1="5.588" x2="-14.478" y2="2.794" width="0.254" layer="94"/>
<wire x1="-14.478" y1="2.794" x2="-15.24" y2="2.794" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.588" x2="6.858" y2="5.588" width="0.254" layer="94"/>
<wire x1="6.858" y1="5.588" x2="6.858" y2="2.794" width="0.254" layer="94"/>
<wire x1="6.858" y1="2.794" x2="7.62" y2="2.794" width="0.254" layer="94"/>
<text x="-14.986" y="11.176" size="1.6764" layer="94">&gt;NAME</text>
<text x="-8.636" y="-10.16" size="1.6764" layer="94">&gt;VALUE</text>
<pin name="G1" x="-20.32" y="5.08" length="middle"/>
<pin name="G2" x="12.7" y="5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCHRAUBKLEMME-PCB-12M4" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHRAUBKLEMME-PCB-12M4" x="-5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="SCHRAUBKLEMME-PCB-12M4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RJ45-MEBP8-8G" uservalue="yes">
<description>https://www.reichelt.de/modular-einbaubuchse-8-8-geschirmt-mebp-8-8g-p24975.html?&amp;trstct=pos_0&lt;br&gt;
Artikel-Nr.: MEBP 8-8G
&lt;br&gt;
&lt;img src="https://cdn-reichelt.de/bilder/web/xxl_ws/G100/MEBP64G2.png"&gt;</description>
<gates>
<gate name="G$1" symbol="RJ45-MEBP8-8G" x="5.08" y="-5.08"/>
</gates>
<devices>
<device name="" package="RJ45-MEBP8-8G">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="G1" pad="G1"/>
<connect gate="G$1" pin="G2" pad="G2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lumberg">
<description>&lt;b&gt;Lumberg Connectors&lt;/b&gt;&lt;p&gt;
include con-mfs.lbr - 2001.03.22&lt;br&gt;
Jack connectors - 2005.11.23&lt;p&gt;
http://www.lumberg.de&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1503_08">
<description>&lt;b&gt;Jack connectors according to JISC 6560, 3.5 mm&lt;/b&gt;&lt;p&gt;
Klinkensteckverbinder nach JISC 6560, 3,5 mm&lt;br&gt;
Source: http://www.lumberg.com/Produkte/PDFs/1503_08.pdf</description>
<wire x1="-3.3" y1="-7.75" x2="-2.9" y2="-7.35" width="0" layer="46" curve="-90"/>
<wire x1="-2.9" y1="-7.35" x2="-2.5" y2="-7.75" width="0" layer="46" curve="-90"/>
<wire x1="-2.5" y1="-7.75" x2="-2.5" y2="-8.25" width="0" layer="46"/>
<wire x1="-2.5" y1="-8.25" x2="-2.9" y2="-8.65" width="0" layer="46" curve="-90"/>
<wire x1="-2.9" y1="-8.65" x2="-3.3" y2="-8.25" width="0" layer="46" curve="-90"/>
<wire x1="-3.3" y1="-8.25" x2="-3.3" y2="-7.75" width="0" layer="46"/>
<wire x1="2.5" y1="-7.75" x2="2.9" y2="-7.35" width="0" layer="46" curve="-90"/>
<wire x1="2.9" y1="-7.35" x2="3.3" y2="-7.75" width="0" layer="46" curve="-90"/>
<wire x1="3.3" y1="-7.75" x2="3.3" y2="-8.25" width="0" layer="46"/>
<wire x1="3.3" y1="-8.25" x2="2.9" y2="-8.65" width="0" layer="46" curve="-90"/>
<wire x1="2.9" y1="-8.65" x2="2.5" y2="-8.25" width="0" layer="46" curve="-90"/>
<wire x1="2.5" y1="-8.25" x2="2.5" y2="-7.75" width="0" layer="46"/>
<wire x1="-3.7" y1="0.15" x2="-3.2" y2="0.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.2" y1="0.65" x2="-2.7" y2="0.15" width="0" layer="46" curve="-90"/>
<wire x1="-2.7" y1="0.15" x2="-2.7" y2="-0.15" width="0" layer="46"/>
<wire x1="-2.7" y1="-0.15" x2="-3.2" y2="-0.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.2" y1="-0.65" x2="-3.7" y2="-0.15" width="0" layer="46" curve="-90"/>
<wire x1="-3.7" y1="-0.15" x2="-3.7" y2="0.15" width="0" layer="46"/>
<wire x1="3.2" y1="-0.65" x2="2.7" y2="-0.15" width="0" layer="46" curve="-90"/>
<wire x1="2.7" y1="-0.15" x2="2.7" y2="0.15" width="0" layer="46"/>
<wire x1="2.7" y1="0.15" x2="3.2" y2="0.65" width="0" layer="46" curve="-90"/>
<wire x1="3.2" y1="0.65" x2="3.7" y2="0.15" width="0" layer="46" curve="-90"/>
<wire x1="3.7" y1="0.15" x2="3.7" y2="-0.15" width="0" layer="46"/>
<wire x1="3.7" y1="-0.15" x2="3.2" y2="-0.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.4" y1="3.4" x2="3.4" y2="3.4" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="1.3" width="0.2032" layer="21"/>
<wire x1="3.4" y1="1.3" x2="3.4" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-1.3" x2="3.4" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-6.5" x2="3.4" y2="-7.9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-7.9" x2="2.1" y2="-7.9" width="0.2032" layer="51"/>
<wire x1="2.1" y1="-7.9" x2="-2.1" y2="-7.9" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="-7.9" x2="-3.4" y2="-7.9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-7.9" x2="-3.4" y2="-6.55" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-6.55" x2="-3.4" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-1.5" x2="-3.4" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="1.45" x2="-3.4" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="5.9" x2="3.1" y2="5.9" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="3.5" x2="-3.1" y2="5.9" width="0.2032" layer="21"/>
<wire x1="3.1" y1="3.5" x2="3.1" y2="5.9" width="0.2032" layer="21"/>
<pad name="1" x="-2.9" y="-8" drill="1" diameter="1.2" shape="long" rot="R90"/>
<pad name="3" x="2.9" y="-8" drill="1" diameter="1.2" shape="long" rot="R90"/>
<pad name="2@1" x="-3.2" y="0" drill="1" diameter="1.2" shape="long" rot="R90"/>
<pad name="2@2" x="3.2" y="0" drill="1" diameter="1.2" shape="long" rot="R90"/>
<text x="-4.452" y="-8.812" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.088" y="-8.812" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<hole x="0" y="4.5" drill="1.2"/>
<hole x="0" y="-5.5" drill="1.2"/>
</package>
<package name="1503_07">
<description>&lt;b&gt;Jack connectors according to JISC 6560, 3.5 mm&lt;/b&gt;&lt;p&gt;
Klinkensteckverbinder nach JISC 6560, 3,5 mm&lt;br&gt;
Source: http://www.lumberg.com/Produkte/PDFs/1503_07.pdf</description>
<wire x1="-3.4" y1="-10.75" x2="-3" y2="-10.35" width="0" layer="46" curve="-90"/>
<wire x1="-3" y1="-10.35" x2="-2.6" y2="-10.75" width="0" layer="46" curve="-90"/>
<wire x1="-2.6" y1="-10.75" x2="-2.6" y2="-11.25" width="0" layer="46"/>
<wire x1="-2.6" y1="-11.25" x2="-3" y2="-11.65" width="0" layer="46" curve="-90"/>
<wire x1="-3" y1="-11.65" x2="-3.4" y2="-11.25" width="0" layer="46" curve="-90"/>
<wire x1="-3.4" y1="-11.25" x2="-3.4" y2="-10.75" width="0" layer="46"/>
<wire x1="2.6" y1="-10.75" x2="3" y2="-10.35" width="0" layer="46" curve="-90"/>
<wire x1="3" y1="-10.35" x2="3.4" y2="-10.75" width="0" layer="46" curve="-90"/>
<wire x1="3.4" y1="-10.75" x2="3.4" y2="-11.25" width="0" layer="46"/>
<wire x1="3.4" y1="-11.25" x2="3" y2="-11.65" width="0" layer="46" curve="-90"/>
<wire x1="3" y1="-11.65" x2="2.6" y2="-11.25" width="0" layer="46" curve="-90"/>
<wire x1="2.6" y1="-11.25" x2="2.6" y2="-10.75" width="0" layer="46"/>
<wire x1="-3.2" y1="-1.85" x2="-2.7" y2="-1.35" width="0" layer="46" curve="-90"/>
<wire x1="-2.7" y1="-1.35" x2="-2.2" y2="-1.85" width="0" layer="46" curve="-90"/>
<wire x1="-2.2" y1="-1.85" x2="-2.2" y2="-2.15" width="0" layer="46"/>
<wire x1="-2.2" y1="-2.15" x2="-2.7" y2="-2.65" width="0" layer="46" curve="-90"/>
<wire x1="-2.7" y1="-2.65" x2="-3.2" y2="-2.15" width="0" layer="46" curve="-90"/>
<wire x1="-3.2" y1="-2.15" x2="-3.2" y2="-1.85" width="0" layer="46"/>
<wire x1="2.7" y1="-2.65" x2="2.2" y2="-2.15" width="0" layer="46" curve="-90"/>
<wire x1="2.2" y1="-2.15" x2="2.2" y2="-1.85" width="0" layer="46"/>
<wire x1="2.2" y1="-1.85" x2="2.7" y2="-1.35" width="0" layer="46" curve="-90"/>
<wire x1="2.7" y1="-1.35" x2="3.2" y2="-1.85" width="0" layer="46" curve="-90"/>
<wire x1="3.2" y1="-1.85" x2="3.2" y2="-2.15" width="0" layer="46"/>
<wire x1="3.2" y1="-2.15" x2="2.7" y2="-2.65" width="0" layer="46" curve="-90"/>
<wire x1="-3.15" y1="2.4" x2="3.15" y2="2.4" width="0.2032" layer="21"/>
<wire x1="3.15" y1="2.4" x2="3.15" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-0.7" x2="3.15" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="3.15" y1="-3.3" x2="3.15" y2="-9.5" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-9.5" x2="3.15" y2="-10.9" width="0.2032" layer="51"/>
<wire x1="3.15" y1="-10.9" x2="2.1" y2="-10.9" width="0.2032" layer="51"/>
<wire x1="2.1" y1="-10.9" x2="-2.1" y2="-10.9" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="-10.9" x2="-3.15" y2="-10.9" width="0.2032" layer="51"/>
<wire x1="-3.15" y1="-10.9" x2="-3.15" y2="-9.55" width="0.2032" layer="51"/>
<wire x1="-3.15" y1="-9.55" x2="-3.15" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-3.5" x2="-3.15" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-3.15" y1="-0.55" x2="-3.15" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="4.9" x2="2.4" y2="4.9" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="2.5" x2="-2.4" y2="4.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="2.5" x2="2.4" y2="4.9" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-11" drill="1" diameter="1.2" shape="long" rot="R90"/>
<pad name="3" x="3" y="-11" drill="1" diameter="1.2" shape="long" rot="R90"/>
<pad name="2@1" x="-2.7" y="-2" drill="1" diameter="1.2" shape="long" rot="R90"/>
<pad name="2@2" x="2.7" y="-2" drill="1" diameter="1.2" shape="long" rot="R90"/>
<text x="-4.452" y="-10.812" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.088" y="-10.812" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<hole x="-2.5" y="-7" drill="1"/>
<hole x="2.5" y="-7" drill="1"/>
</package>
</packages>
<symbols>
<symbol name="JACK-STEREO-2">
<wire x1="0" y1="2.54" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.016" x2="2.032" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0.508" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-5.08" x2="4.064" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-1.524" x2="4.572" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="2.54" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="3.048" y2="-1.524" width="0.1524" layer="94"/>
<text x="-2.54" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="5.207" y1="-2.667" x2="6.604" y2="2.667" layer="94"/>
<pin name="1" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-5.08" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="2@1" x="-2.54" y="2.54" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1503_?" prefix="X">
<description>&lt;b&gt;Jack connectors according to JISC 6560, 3.5 mm&lt;/b&gt;&lt;p&gt;
Klinkensteckverbinder nach JISC 6560, 3,5 mm&lt;br&gt;
Source: http://www.lumberg.com/Produkte/PDFs/1503_08.pdf</description>
<gates>
<gate name="G$1" symbol="JACK-STEREO-2" x="0" y="0"/>
</gates>
<devices>
<device name="08" package="1503_08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2@1"/>
<connect gate="G$1" pin="2@1" pad="2@2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="07" package="1503_07">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2@1"/>
<connect gate="G$1" pin="2@1" pad="2@2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="SV2" library="con-lstb" deviceset="MA04-2" device=""/>
<part name="MINI-DIN-8P" library="mini_din" deviceset="MINI_DIN_8" device="" value="CAT"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SV1" library="con-lstb" deviceset="MA04-2" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="MIN-DIN-6P" library="mini_din" deviceset="MINI_DIN_6" device="" value="DATA"/>
<part name="MC-IN" library="DIGIBOX" deviceset="RJ45-MEBP8-8G" device="" value="MIC_IN"/>
<part name="MC-OUT" library="DIGIBOX" deviceset="RJ45-MEBP8-8G" device="" value="MIC_OUT"/>
<part name="MIC-IO" library="con-lstb" deviceset="MA04-2" device="" value="MIC_IO"/>
<part name="KLEMME1" library="DIGIBOX" deviceset="SCHRAUBKLEMME-PCB-12M4" device="" value="Klemme 1"/>
<part name="KLEMME2" library="DIGIBOX" deviceset="SCHRAUBKLEMME-PCB-12M4" device="" value="Klemme 2"/>
<part name="FRAME1" library="frames" deviceset="A3L-LOC" device=""/>
<part name="F-PTT" library="con-lumberg" deviceset="1503_?" device="07" value="LUM 1503_07"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="67.31" y="57.912" size="2.1844" layer="94" font="vector">1
2
3
4
5
6
7
8</text>
<text x="72.39" y="57.912" size="2.1844" layer="94" font="vector">13,8V
TX GND
GND
TX D
RX D
BAND C
RESET
TX INH</text>
<text x="142.24" y="58.42" size="2.1844" layer="94" font="vector">1
2
3
-
5
6
-
8</text>
<text x="147.32" y="58.42" size="2.1844" layer="94" font="vector">DATA IN
GND
PTT
-
DATA OUT 9600bps
DATA OUT 1200bps
-
SQL</text>
<wire x1="102.87" y1="83.312" x2="102.87" y2="80.01" width="0.1524" layer="94"/>
<wire x1="102.87" y1="80.01" x2="64.77" y2="80.01" width="0.1524" layer="94"/>
<wire x1="64.77" y1="80.01" x2="64.77" y2="76.962" width="0.1524" layer="94"/>
<wire x1="64.77" y1="76.962" x2="102.87" y2="76.962" width="0.1524" layer="94"/>
<wire x1="102.87" y1="76.962" x2="102.87" y2="73.66" width="0.1524" layer="94"/>
<wire x1="102.87" y1="73.66" x2="64.77" y2="73.66" width="0.1524" layer="94"/>
<wire x1="64.77" y1="73.66" x2="64.77" y2="70.612" width="0.1524" layer="94"/>
<wire x1="64.77" y1="70.612" x2="102.87" y2="70.612" width="0.1524" layer="94"/>
<wire x1="102.87" y1="70.612" x2="102.87" y2="67.31" width="0.1524" layer="94"/>
<wire x1="102.87" y1="67.31" x2="64.77" y2="67.31" width="0.1524" layer="94"/>
<wire x1="64.77" y1="67.31" x2="64.77" y2="64.008" width="0.1524" layer="94"/>
<wire x1="64.77" y1="64.008" x2="102.87" y2="64.008" width="0.1524" layer="94"/>
<wire x1="102.87" y1="64.008" x2="102.87" y2="60.706" width="0.1524" layer="94"/>
<wire x1="102.87" y1="60.706" x2="64.77" y2="60.706" width="0.1524" layer="94"/>
<wire x1="64.77" y1="60.706" x2="64.77" y2="57.15" width="0.1524" layer="94"/>
<wire x1="64.77" y1="57.15" x2="70.612" y2="57.15" width="0.1524" layer="94"/>
<wire x1="70.612" y1="57.15" x2="102.87" y2="57.15" width="0.1524" layer="94"/>
<wire x1="102.87" y1="57.15" x2="102.87" y2="80.01" width="0.1524" layer="94"/>
<wire x1="102.87" y1="83.312" x2="70.612" y2="83.312" width="0.1524" layer="94"/>
<wire x1="70.612" y1="83.312" x2="64.77" y2="83.312" width="0.1524" layer="94"/>
<wire x1="64.77" y1="83.312" x2="64.77" y2="60.706" width="0.1524" layer="94"/>
<wire x1="70.612" y1="83.312" x2="70.612" y2="57.15" width="0.1524" layer="94"/>
<wire x1="177.8" y1="83.82" x2="177.8" y2="80.518" width="0.1524" layer="94"/>
<wire x1="177.8" y1="80.518" x2="139.7" y2="80.518" width="0.1524" layer="94"/>
<wire x1="139.7" y1="77.47" x2="177.8" y2="77.47" width="0.1524" layer="94"/>
<wire x1="177.8" y1="77.47" x2="177.8" y2="74.168" width="0.1524" layer="94"/>
<wire x1="177.8" y1="74.168" x2="139.7" y2="74.168" width="0.1524" layer="94"/>
<wire x1="139.7" y1="71.12" x2="177.8" y2="71.12" width="0.1524" layer="94"/>
<wire x1="177.8" y1="71.12" x2="177.8" y2="67.818" width="0.1524" layer="94"/>
<wire x1="177.8" y1="67.818" x2="139.7" y2="67.818" width="0.1524" layer="94"/>
<wire x1="139.7" y1="64.516" x2="177.8" y2="64.516" width="0.1524" layer="94"/>
<wire x1="177.8" y1="64.516" x2="177.8" y2="61.214" width="0.1524" layer="94"/>
<wire x1="177.8" y1="61.214" x2="139.7" y2="61.214" width="0.1524" layer="94"/>
<wire x1="139.7" y1="57.658" x2="145.542" y2="57.658" width="0.1524" layer="94"/>
<wire x1="145.542" y1="57.658" x2="177.8" y2="57.658" width="0.1524" layer="94"/>
<wire x1="177.8" y1="57.658" x2="177.8" y2="80.518" width="0.1524" layer="94"/>
<wire x1="177.8" y1="83.82" x2="145.542" y2="83.82" width="0.1524" layer="94"/>
<wire x1="139.7" y1="83.82" x2="139.7" y2="57.658" width="0.1524" layer="94"/>
<wire x1="139.7" y1="83.82" x2="145.542" y2="83.82" width="0.1524" layer="94"/>
<wire x1="145.542" y1="83.82" x2="145.542" y2="57.658" width="0.1524" layer="94"/>
<text x="64.77" y="84.328" size="2.1844" layer="94" font="vector">CAT     MINI-DIN-8P</text>
<text x="139.7" y="84.836" size="2.1844" layer="94" font="vector">DATA    MINI-DIN-6P</text>
<text x="223.52" y="58.42" size="2.1844" layer="94" font="vector">1
2
3
4
5
6
7
8</text>
<text x="228.6" y="58.42" size="2.1844" layer="94" font="vector">TXD
TXR
+5V
MIC GND
MIC
PTT
MIC-PTT-GND
FAST</text>
<wire x1="248.92" y1="83.82" x2="248.92" y2="80.518" width="0.1524" layer="94"/>
<wire x1="248.92" y1="80.518" x2="220.98" y2="80.518" width="0.1524" layer="94"/>
<wire x1="220.98" y1="77.47" x2="248.92" y2="77.47" width="0.1524" layer="94"/>
<wire x1="248.92" y1="77.47" x2="248.92" y2="74.168" width="0.1524" layer="94"/>
<wire x1="248.92" y1="74.168" x2="220.98" y2="74.168" width="0.1524" layer="94"/>
<wire x1="220.98" y1="71.12" x2="248.92" y2="71.12" width="0.1524" layer="94"/>
<wire x1="248.92" y1="71.12" x2="248.92" y2="67.818" width="0.1524" layer="94"/>
<wire x1="248.92" y1="67.818" x2="220.98" y2="67.818" width="0.1524" layer="94"/>
<wire x1="220.98" y1="64.516" x2="248.92" y2="64.516" width="0.1524" layer="94"/>
<wire x1="248.92" y1="64.516" x2="248.92" y2="61.214" width="0.1524" layer="94"/>
<wire x1="248.92" y1="61.214" x2="220.98" y2="61.214" width="0.1524" layer="94"/>
<wire x1="220.98" y1="57.658" x2="226.822" y2="57.658" width="0.1524" layer="94"/>
<wire x1="226.822" y1="57.658" x2="248.92" y2="57.658" width="0.1524" layer="94"/>
<wire x1="248.92" y1="57.658" x2="248.92" y2="80.518" width="0.1524" layer="94"/>
<wire x1="248.92" y1="83.82" x2="226.822" y2="83.82" width="0.1524" layer="94"/>
<wire x1="220.98" y1="83.82" x2="220.98" y2="57.658" width="0.1524" layer="94"/>
<wire x1="220.98" y1="83.82" x2="226.822" y2="83.82" width="0.1524" layer="94"/>
<wire x1="226.822" y1="83.82" x2="226.822" y2="57.658" width="0.1524" layer="94"/>
<text x="220.98" y="84.836" size="2.1844" layer="94" font="vector">MIC RJ45</text>
<text x="279.4" y="-96.52" size="6.4516" layer="94">DK8DE</text>
</plain>
<instances>
<instance part="SV2" gate="G$1" x="88.9" y="12.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="78.74" y="16.51" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="94.742" y="16.51" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="MINI-DIN-8P" gate="G$1" x="86.36" y="40.64" smashed="yes">
<attribute name="VALUE" x="92.71" y="51.562" size="1.778" layer="96"/>
<attribute name="NAME" x="76.2" y="51.435" size="1.778" layer="95"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="73.66" y="25.4" smashed="yes">
<attribute name="VALUE" x="75.565" y="24.257" size="1.778" layer="96"/>
</instance>
<instance part="SV1" gate="G$1" x="162.56" y="12.7" smashed="yes" rot="R270">
<attribute name="VALUE" x="152.4" y="16.51" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="168.402" y="16.51" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="147.32" y="25.4" smashed="yes">
<attribute name="VALUE" x="149.225" y="24.257" size="1.778" layer="96"/>
</instance>
<instance part="MIN-DIN-6P" gate="G$1" x="160.02" y="38.1" smashed="yes">
<attribute name="VALUE" x="164.084" y="51.435" size="1.778" layer="96"/>
<attribute name="NAME" x="149.86" y="51.435" size="1.778" layer="95"/>
</instance>
<instance part="MC-IN" gate="G$1" x="205.74" y="15.24" smashed="yes" rot="R270">
<attribute name="NAME" x="200.914" y="34.036" size="1.6764" layer="94"/>
<attribute name="VALUE" x="195.58" y="23.876" size="1.6764" layer="94" rot="R270"/>
</instance>
<instance part="MC-OUT" gate="G$1" x="276.86" y="15.24" smashed="yes" rot="R270">
<attribute name="NAME" x="269.494" y="34.036" size="1.6764" layer="94"/>
<attribute name="VALUE" x="266.7" y="23.876" size="1.6764" layer="94" rot="R270"/>
</instance>
<instance part="MIC-IO" gate="G$1" x="236.22" y="45.72" smashed="yes">
<attribute name="VALUE" x="232.41" y="35.56" size="1.778" layer="96"/>
<attribute name="NAME" x="232.41" y="51.562" size="1.778" layer="95"/>
</instance>
<instance part="KLEMME1" gate="G$1" x="259.08" y="-10.16" smashed="yes">
<attribute name="NAME" x="254" y="-1.778" size="1.27" layer="95"/>
<attribute name="VALUE" x="254" y="-24.892" size="1.27" layer="96"/>
</instance>
<instance part="KLEMME2" gate="G$1" x="274.32" y="-10.16" smashed="yes">
<attribute name="NAME" x="269.24" y="-1.778" size="1.27" layer="95"/>
<attribute name="VALUE" x="269.24" y="-24.892" size="1.27" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-17.78" y="-106.68" smashed="yes">
<attribute name="DRAWING_NAME" x="326.39" y="-91.44" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="326.39" y="-96.52" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="339.725" y="-101.6" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="F-PTT" gate="G$1" x="304.8" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="300.736" y="22.86" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="313.69" y="22.86" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND-CAT" class="0">
<segment>
<wire x1="101.6" y1="40.64" x2="104.14" y2="40.64" width="0.1524" layer="91"/>
<wire x1="104.14" y1="40.64" x2="104.14" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-7.62" x2="86.36" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="3"/>
<wire x1="86.36" y1="-7.62" x2="86.36" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="3"/>
<label x="104.14" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="13,8V" class="0">
<segment>
<wire x1="101.6" y1="2.54" x2="101.6" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="1"/>
<wire x1="101.6" y1="2.54" x2="83.82" y2="2.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="2.54" x2="83.82" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="1"/>
<label x="101.6" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RX-D" class="0">
<segment>
<wire x1="71.12" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="40.64" x2="68.58" y2="0" width="0.1524" layer="91"/>
<wire x1="68.58" y1="0" x2="88.9" y2="0" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="5"/>
<wire x1="88.9" y1="0" x2="88.9" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="5"/>
<label x="68.58" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TX-GND" class="0">
<segment>
<wire x1="71.12" y1="35.56" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="2"/>
<wire x1="71.12" y1="22.86" x2="83.82" y2="22.86" width="0.1524" layer="91"/>
<wire x1="83.82" y1="22.86" x2="83.82" y2="20.32" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="2"/>
<label x="71.12" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="BAND-C" class="0">
<segment>
<wire x1="99.06" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="43.18" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="6"/>
<wire x1="88.9" y1="20.32" x2="88.9" y2="22.86" width="0.1524" layer="91"/>
<wire x1="88.9" y1="22.86" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="6"/>
<label x="109.22" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TX-INH" class="0">
<segment>
<pinref part="SV2" gate="G$1" pin="8"/>
<wire x1="91.44" y1="20.32" x2="99.06" y2="20.32" width="0.1524" layer="91"/>
<wire x1="99.06" y1="20.32" x2="99.06" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-2.54" x2="66.04" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-2.54" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="8"/>
<label x="66.04" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TX-D" class="0">
<segment>
<wire x1="99.06" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<wire x1="106.68" y1="38.1" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
<wire x1="106.68" y1="25.4" x2="86.36" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="4"/>
<wire x1="86.36" y1="25.4" x2="86.36" y2="20.32" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="4"/>
<label x="106.68" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="71.12" y1="45.72" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<wire x1="63.5" y1="45.72" x2="63.5" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-5.08" x2="91.44" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="7"/>
<wire x1="91.44" y1="-5.08" x2="91.44" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="7"/>
<label x="63.5" y="25.4" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="MINI-DIN-8P" gate="G$1" pin="PE3"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="73.66" y1="33.02" x2="73.66" y2="27.94" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="PE2"/>
<wire x1="73.66" y1="27.94" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<junction x="73.66" y="27.94"/>
<pinref part="MINI-DIN-8P" gate="G$1" pin="PE1"/>
<wire x1="86.36" y1="27.94" x2="99.06" y2="27.94" width="0.1524" layer="91"/>
<wire x1="99.06" y1="27.94" x2="99.06" y2="33.02" width="0.1524" layer="91"/>
<junction x="86.36" y="27.94"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="147.32" y1="33.02" x2="147.32" y2="27.94" width="0.1524" layer="91"/>
<junction x="147.32" y="27.94"/>
<wire x1="147.32" y1="27.94" x2="160.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="160.02" y1="27.94" x2="172.72" y2="27.94" width="0.1524" layer="91"/>
<wire x1="172.72" y1="27.94" x2="172.72" y2="33.02" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="PE1"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="PE3"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="PE2"/>
<junction x="160.02" y="27.94"/>
</segment>
<segment>
<pinref part="MC-IN" gate="G$1" pin="G2"/>
<pinref part="MC-OUT" gate="G$1" pin="G2"/>
<wire x1="210.82" y1="2.54" x2="281.94" y2="2.54" width="0.1524" layer="91"/>
<label x="213.36" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MC-IN" gate="G$1" pin="G1"/>
<pinref part="MC-OUT" gate="G$1" pin="G1"/>
<wire x1="210.82" y1="35.56" x2="281.94" y2="35.56" width="0.1524" layer="91"/>
<label x="213.36" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PTT-6P" class="0">
<segment>
<wire x1="175.26" y1="40.64" x2="177.8" y2="40.64" width="0.1524" layer="91"/>
<wire x1="177.8" y1="40.64" x2="177.8" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-7.62" x2="160.02" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="SV1" gate="G$1" pin="3"/>
<wire x1="160.02" y1="-7.62" x2="160.02" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="3"/>
<label x="177.8" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DATA-IN" class="0">
<segment>
<wire x1="175.26" y1="2.54" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<pinref part="SV1" gate="G$1" pin="1"/>
<wire x1="175.26" y1="2.54" x2="157.48" y2="2.54" width="0.1524" layer="91"/>
<wire x1="157.48" y1="2.54" x2="157.48" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="1"/>
<label x="175.26" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DATA-OUT-9600BPS" class="0">
<segment>
<wire x1="144.78" y1="40.64" x2="142.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="142.24" y1="40.64" x2="142.24" y2="0" width="0.1524" layer="91"/>
<wire x1="142.24" y1="0" x2="162.56" y2="0" width="0.1524" layer="91"/>
<pinref part="SV1" gate="G$1" pin="5"/>
<wire x1="162.56" y1="0" x2="162.56" y2="5.08" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="5"/>
<label x="142.24" y="15.24" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="GND-6P" class="0">
<segment>
<wire x1="144.78" y1="35.56" x2="144.78" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SV1" gate="G$1" pin="2"/>
<wire x1="144.78" y1="22.86" x2="157.48" y2="22.86" width="0.1524" layer="91"/>
<wire x1="157.48" y1="22.86" x2="157.48" y2="20.32" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="2"/>
<label x="144.78" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DATA-OUT-1200BPS" class="0">
<segment>
<wire x1="172.72" y1="43.18" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
<wire x1="182.88" y1="43.18" x2="182.88" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SV1" gate="G$1" pin="6"/>
<wire x1="162.56" y1="20.32" x2="162.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="162.56" y1="22.86" x2="182.88" y2="22.86" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="6"/>
<label x="185.42" y="22.86" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SQL" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="8"/>
<wire x1="165.1" y1="20.32" x2="172.72" y2="20.32" width="0.1524" layer="91"/>
<wire x1="172.72" y1="20.32" x2="172.72" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-2.54" x2="139.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-2.54" x2="139.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="139.7" y1="43.18" x2="147.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="MIN-DIN-6P" gate="G$1" pin="8"/>
<label x="139.7" y="27.94" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="1"/>
<label x="254" y="27.94" size="1.778" layer="95"/>
<pinref part="MC-OUT" gate="G$1" pin="1"/>
<wire x1="205.74" y1="27.94" x2="243.84" y2="27.94" width="0.1524" layer="91"/>
<pinref part="MIC-IO" gate="G$1" pin="1"/>
<wire x1="243.84" y1="27.94" x2="276.86" y2="27.94" width="0.1524" layer="91"/>
<wire x1="243.84" y1="40.64" x2="243.84" y2="27.94" width="0.1524" layer="91"/>
<junction x="243.84" y="27.94"/>
</segment>
</net>
<net name="TXR" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="2"/>
<pinref part="MC-OUT" gate="G$1" pin="2"/>
<wire x1="205.74" y1="25.4" x2="228.6" y2="25.4" width="0.1524" layer="91"/>
<label x="254" y="25.4" size="1.778" layer="95"/>
<pinref part="MIC-IO" gate="G$1" pin="2"/>
<wire x1="228.6" y1="25.4" x2="276.86" y2="25.4" width="0.1524" layer="91"/>
<wire x1="228.6" y1="40.64" x2="228.6" y2="25.4" width="0.1524" layer="91"/>
<junction x="228.6" y="25.4"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="3"/>
<label x="254" y="22.86" size="1.778" layer="95"/>
<pinref part="MC-OUT" gate="G$1" pin="3"/>
<wire x1="276.86" y1="22.86" x2="246.38" y2="22.86" width="0.1524" layer="91"/>
<pinref part="MIC-IO" gate="G$1" pin="3"/>
<wire x1="246.38" y1="22.86" x2="205.74" y2="22.86" width="0.1524" layer="91"/>
<wire x1="243.84" y1="43.18" x2="246.38" y2="43.18" width="0.1524" layer="91"/>
<wire x1="246.38" y1="43.18" x2="246.38" y2="22.86" width="0.1524" layer="91"/>
<junction x="246.38" y="22.86"/>
</segment>
</net>
<net name="MIC_GND" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="4"/>
<pinref part="MC-OUT" gate="G$1" pin="4"/>
<wire x1="205.74" y1="20.32" x2="226.06" y2="20.32" width="0.1524" layer="91"/>
<label x="254" y="20.32" size="1.778" layer="95"/>
<pinref part="MIC-IO" gate="G$1" pin="4"/>
<wire x1="226.06" y1="20.32" x2="276.86" y2="20.32" width="0.1524" layer="91"/>
<wire x1="228.6" y1="43.18" x2="226.06" y2="43.18" width="0.1524" layer="91"/>
<wire x1="226.06" y1="43.18" x2="226.06" y2="20.32" width="0.1524" layer="91"/>
<junction x="226.06" y="20.32"/>
</segment>
</net>
<net name="MIC" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="5"/>
<label x="254" y="17.78" size="1.778" layer="95"/>
<pinref part="MC-OUT" gate="G$1" pin="5"/>
<wire x1="205.74" y1="17.78" x2="248.92" y2="17.78" width="0.1524" layer="91"/>
<pinref part="MIC-IO" gate="G$1" pin="5"/>
<wire x1="248.92" y1="17.78" x2="276.86" y2="17.78" width="0.1524" layer="91"/>
<wire x1="243.84" y1="45.72" x2="248.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="248.92" y1="45.72" x2="248.92" y2="17.78" width="0.1524" layer="91"/>
<junction x="248.92" y="17.78"/>
</segment>
</net>
<net name="PTT" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="6"/>
<pinref part="MC-OUT" gate="G$1" pin="6"/>
<wire x1="205.74" y1="15.24" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<label x="254" y="15.24" size="1.778" layer="95"/>
<pinref part="MIC-IO" gate="G$1" pin="6"/>
<wire x1="223.52" y1="15.24" x2="276.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="228.6" y1="45.72" x2="223.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="223.52" y1="45.72" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<junction x="223.52" y="15.24"/>
<junction x="276.86" y="15.24"/>
<pinref part="F-PTT" gate="G$1" pin="2"/>
<wire x1="302.26" y1="15.24" x2="276.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="302.26" y1="20.32" x2="302.26" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAST" class="0">
<segment>
<pinref part="MC-OUT" gate="G$1" pin="8"/>
<pinref part="MC-IN" gate="G$1" pin="8"/>
<wire x1="276.86" y1="10.16" x2="220.98" y2="10.16" width="0.1524" layer="91"/>
<label x="254" y="10.16" size="1.778" layer="95"/>
<pinref part="MIC-IO" gate="G$1" pin="8"/>
<wire x1="220.98" y1="10.16" x2="205.74" y2="10.16" width="0.1524" layer="91"/>
<wire x1="228.6" y1="48.26" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<wire x1="220.98" y1="48.26" x2="220.98" y2="10.16" width="0.1524" layer="91"/>
<junction x="220.98" y="10.16"/>
</segment>
</net>
<net name="MIC-PTT-GND" class="0">
<segment>
<pinref part="MC-IN" gate="G$1" pin="7"/>
<pinref part="MC-OUT" gate="G$1" pin="7"/>
<wire x1="205.74" y1="12.7" x2="276.86" y2="12.7" width="0.1524" layer="91"/>
<label x="254" y="12.7" size="1.778" layer="95"/>
<pinref part="MIC-IO" gate="G$1" pin="7"/>
<wire x1="251.46" y1="12.7" x2="276.86" y2="12.7" width="0.1524" layer="91"/>
<wire x1="243.84" y1="48.26" x2="251.46" y2="48.26" width="0.1524" layer="91"/>
<wire x1="251.46" y1="48.26" x2="251.46" y2="12.7" width="0.1524" layer="91"/>
<junction x="251.46" y="12.7"/>
<wire x1="309.88" y1="12.7" x2="276.86" y2="12.7" width="0.1524" layer="91"/>
<junction x="276.86" y="12.7"/>
<pinref part="F-PTT" gate="G$1" pin="1"/>
<wire x1="309.88" y1="12.7" x2="309.88" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
