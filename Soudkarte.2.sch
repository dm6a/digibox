<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Mechanical" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Gehäuse" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="Stiffner" color="6" fill="3" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="no"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="no"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="DIGIBOX">
<packages>
<package name="MPE-014">
<pad name="1" x="-10.16" y="-2.54" drill="1"/>
<pad name="2" x="-10.16" y="0" drill="1"/>
<pad name="3" x="-7.62" y="-2.54" drill="1"/>
<pad name="4" x="-7.62" y="0" drill="1"/>
<pad name="5" x="-5.08" y="-2.54" drill="1"/>
<pad name="6" x="-5.08" y="0" drill="1"/>
<pad name="7" x="-2.54" y="-2.54" drill="1"/>
<pad name="8" x="-2.54" y="0" drill="1"/>
<pad name="9" x="0" y="-2.54" drill="1"/>
<pad name="10" x="0" y="0" drill="1"/>
<pad name="11" x="2.54" y="-2.54" drill="1"/>
<pad name="12" x="2.54" y="0" drill="1"/>
<pad name="13" x="5.08" y="-2.54" drill="1"/>
<pad name="14" x="5.08" y="0" drill="1"/>
<pad name="15" x="7.62" y="-2.54" drill="1"/>
<pad name="16" x="7.62" y="0" drill="1"/>
<pad name="17" x="10.16" y="-2.54" drill="1"/>
<pad name="18" x="10.16" y="0" drill="1"/>
<pad name="19" x="12.7" y="-2.54" drill="1"/>
<pad name="20" x="12.7" y="0" drill="1"/>
<pad name="21" x="15.24" y="-2.54" drill="1"/>
<pad name="22" x="15.24" y="0" drill="1"/>
<pad name="23" x="17.78" y="-2.54" drill="1"/>
<pad name="24" x="17.78" y="0" drill="1"/>
<pad name="25" x="20.32" y="-2.54" drill="1"/>
<pad name="26" x="20.32" y="0" drill="1"/>
<pad name="27" x="22.86" y="-2.54" drill="1"/>
<pad name="28" x="22.86" y="0" drill="1"/>
<wire x1="-11.43" y1="1.27" x2="24.13" y2="1.27" width="0.127" layer="21"/>
<wire x1="24.13" y1="1.27" x2="24.13" y2="-3.81" width="0.127" layer="21"/>
<wire x1="24.13" y1="-3.81" x2="-11.43" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-3.81" x2="-11.43" y2="1.27" width="0.127" layer="21"/>
<text x="-10.16" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-10.16" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MP-010">
<pad name="2" x="-2.54" y="2.54" drill="0.8"/>
<pad name="4" x="0" y="2.54" drill="0.8"/>
<pad name="6" x="2.54" y="2.54" drill="0.8"/>
<pad name="8" x="5.08" y="2.54" drill="0.8"/>
<pad name="10" x="7.62" y="2.54" drill="0.8"/>
<pad name="9" x="7.62" y="0" drill="0.8"/>
<pad name="7" x="5.08" y="0" drill="0.8"/>
<pad name="5" x="2.54" y="0" drill="0.8"/>
<pad name="3" x="0" y="0" drill="0.8"/>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<wire x1="-3.81" y1="3.81" x2="8.89" y2="3.81" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.81" x2="8.89" y2="-1.27" width="0.127" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<text x="-3.81" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="21">&gt;VALUE</text>
<text x="-5.08" y="-2.54" size="1.27" layer="21">1</text>
<text x="9.525" y="3.175" size="1.27" layer="21">10</text>
</package>
</packages>
<symbols>
<symbol name="MPE-014">
<pin name="1" x="-20.32" y="-7.62" length="short" rot="R90"/>
<pin name="2" x="-20.32" y="7.62" length="short" rot="R270"/>
<pin name="3" x="-17.78" y="-7.62" length="short" rot="R90"/>
<pin name="4" x="-17.78" y="7.62" length="short" rot="R270"/>
<pin name="5" x="-15.24" y="-7.62" length="short" rot="R90"/>
<pin name="6" x="-15.24" y="7.62" length="short" rot="R270"/>
<pin name="7" x="-12.7" y="-7.62" length="short" rot="R90"/>
<pin name="8" x="-12.7" y="7.62" length="short" rot="R270"/>
<pin name="9" x="-10.16" y="-7.62" length="short" rot="R90"/>
<pin name="10" x="-10.16" y="7.62" length="short" rot="R270"/>
<pin name="11" x="-7.62" y="-7.62" length="short" rot="R90"/>
<pin name="12" x="-7.62" y="7.62" length="short" rot="R270"/>
<pin name="13" x="-5.08" y="-7.62" length="short" rot="R90"/>
<pin name="14" x="-5.08" y="7.62" length="short" rot="R270"/>
<pin name="15" x="-2.54" y="-7.62" length="short" rot="R90"/>
<pin name="16" x="-2.54" y="7.62" length="short" rot="R270"/>
<pin name="17" x="0" y="-7.62" length="short" rot="R90"/>
<pin name="18" x="0" y="7.62" length="short" rot="R270"/>
<pin name="19" x="2.54" y="-7.62" length="short" rot="R90"/>
<pin name="20" x="2.54" y="7.62" length="short" rot="R270"/>
<pin name="21" x="5.08" y="-7.62" length="short" rot="R90"/>
<pin name="22" x="5.08" y="7.62" length="short" rot="R270"/>
<pin name="23" x="7.62" y="-7.62" length="short" rot="R90"/>
<pin name="24" x="7.62" y="7.62" length="short" rot="R270"/>
<pin name="25" x="10.16" y="-7.62" length="short" rot="R90"/>
<pin name="26" x="10.16" y="7.62" length="short" rot="R270"/>
<pin name="27" x="12.7" y="-7.62" length="short" rot="R90"/>
<pin name="28" x="12.7" y="7.62" length="short" rot="R270"/>
<wire x1="-21.844" y1="5.08" x2="13.97" y2="5.08" width="0.254" layer="94"/>
<wire x1="13.97" y1="-5.08" x2="-21.844" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-21.844" y1="-5.08" x2="-21.844" y2="5.08" width="0.254" layer="94"/>
<wire x1="13.97" y1="4.826" x2="13.97" y2="-5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="MP-010">
<pin name="2" x="-7.62" y="5.08" length="middle" rot="R270"/>
<pin name="4" x="-5.08" y="5.08" length="middle" rot="R270"/>
<pin name="6" x="-2.54" y="5.08" length="middle" rot="R270"/>
<pin name="8" x="0" y="5.08" length="middle" rot="R270"/>
<pin name="10" x="2.54" y="5.08" length="middle" rot="R270"/>
<pin name="1" x="-7.62" y="-15.24" length="middle" rot="R90"/>
<pin name="3" x="-5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="5" x="-2.54" y="-15.24" length="middle" rot="R90"/>
<pin name="7" x="0" y="-15.24" length="middle" rot="R90"/>
<pin name="9" x="2.54" y="-15.24" length="middle" rot="R90"/>
<wire x1="-10.16" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="0" width="0.254" layer="94"/>
<text x="6.35" y="-2.286" size="1.27" layer="94">&gt;NAME</text>
<text x="6.096" y="-10.16" size="1.27" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MPE-014">
<description>Reichelt MPE 087-2-014</description>
<gates>
<gate name="G$1" symbol="MPE-014" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="MPE-014">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MPE-010" uservalue="yes">
<gates>
<gate name="G$1" symbol="MP-010" x="2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="MP-010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="OUTPUT" library="DIGIBOX" deviceset="MPE-014" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device="" value="A4"/>
<part name="U$1" library="DIGIBOX" deviceset="MPE-010" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="160.02" y="10.16" size="1.778" layer="95"></text>
</plain>
<instances>
<instance part="OUTPUT" gate="G$1" x="408.94" y="58.42" smashed="yes" rot="R270"/>
<instance part="FRAME1" gate="G$1" x="213.36" y="-12.7" smashed="yes">
<attribute name="DRAWING_NAME" x="430.53" y="2.54" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="430.53" y="-2.54" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="443.865" y="-7.62" size="2.54" layer="94" font="vector"/>
</instance>
<instance part="U$1" gate="G$1" x="238.76" y="93.98" smashed="yes" rot="MR270">
<attribute name="NAME" x="241.046" y="87.63" size="1.27" layer="94" rot="MR270"/>
<attribute name="VALUE" x="248.92" y="87.884" size="1.27" layer="94" rot="MR270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
