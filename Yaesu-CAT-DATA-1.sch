<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA04-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<text x="-4.318" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="3.81" y="2.921" size="1.27" layer="21" ratio="10">8</text>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA04-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA04-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA04-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="mini_din">
<packages>
<package name="M_DIN8">
<wire x1="7" y1="4.7" x2="-7" y2="4.7" width="0.127" layer="21"/>
<wire x1="-7" y1="4.7" x2="-7" y2="0.2" width="0.127" layer="21"/>
<wire x1="-7" y1="0.2" x2="-7" y2="-8.1" width="0.127" layer="21"/>
<wire x1="-7" y1="-8.1" x2="7" y2="-8.1008" width="0.127" layer="21"/>
<wire x1="7" y1="-8.1008" x2="7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="7" y1="-1.8" x2="7" y2="0.2" width="0.127" layer="21"/>
<wire x1="7" y1="0.2" x2="7" y2="4.7" width="0.127" layer="21"/>
<wire x1="7" y1="0.2" x2="6.7" y2="0.2" width="0.127" layer="21"/>
<wire x1="6.7" y1="0.2" x2="6.7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="6.7" y1="-1.8" x2="7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-7" y1="0.2" x2="-6.7" y2="0.2" width="0.127" layer="21"/>
<wire x1="-6.7" y1="0.2" x2="-6.7" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-6.7" y1="-1.8" x2="-6.95" y2="-1.8" width="0.127" layer="21"/>
<pad name="SH1" x="6.8" y="-0.8" drill="2.3" diameter="3.5"/>
<pad name="SH3" x="-6.75" y="-0.8" drill="2.3" diameter="3.5"/>
<pad name="SH2" x="0" y="0" drill="2.3" diameter="3.5"/>
<pad name="2" x="1.3" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="1" x="-1.3" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="3" x="-3.4" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="5" x="3.4" y="-3.8" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="8" x="3.4" y="-6.3" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="7" x="0.9" y="-6.3" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="4" x="-1.3" y="-6.3" drill="0.9" diameter="1.4224" shape="octagon"/>
<pad name="6" x="-3.4" y="-6.25" drill="0.9" diameter="1.4224" shape="octagon"/>
<text x="-2.286" y="-5.08" size="0.8128" layer="21">1</text>
<text x="0.092" y="-5.08" size="0.8128" layer="21">2</text>
<text x="-4.58" y="-5.08" size="0.8128" layer="21">3</text>
<text x="-2.498" y="-7.62" size="0.8128" layer="21">4</text>
<text x="2.28" y="-5.12" size="0.8128" layer="21">5</text>
<text x="-4.656" y="-7.58" size="0.8128" layer="21">6</text>
<text x="-0.28" y="-7.62" size="0.8128" layer="21">7</text>
<text x="2.112" y="-7.62" size="0.8128" layer="21">8</text>
<text x="-6.35" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="M_DIN8">
<wire x1="-2.667" y1="-4.699" x2="-2.667" y2="-3.429" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="-3.429" x2="2.54" y2="-4.699" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.635" y1="0.381" x2="0.635" y2="0.381" width="0.254" layer="94" curve="180"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.35" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="0" x2="-5.715" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.874" y1="-5.08" x2="-3.302" y2="-4.064" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.524" x2="5.08" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.524" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.715" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="8.89" x2="1.27" y2="8.89" width="0.1524" layer="94"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="6.35" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-4.064" x2="7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.254" layer="94" curve="-180"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="3.175" width="0.254" layer="94" curve="-180"/>
<wire x1="-0.635" y1="2.794" x2="0.635" y2="2.794" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="3.429" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-8.89" y1="-8.89" x2="0" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="0" y1="-8.89" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="-8.89" x2="8.89" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.508" x2="0" y2="-1.524" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="7.62" width="0.8128" layer="94"/>
<text x="0" y="10.795" size="1.778" layer="96">&gt;VALUE</text>
<text x="-10.16" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-1.651" size="1.778" layer="94">4</text>
<text x="-3.302" y="1.778" size="1.778" layer="94">8</text>
<text x="5.969" y="-8.255" size="1.524" layer="94">PE</text>
<text x="-8.636" y="-8.255" size="1.524" layer="94">PE</text>
<text x="0.889" y="-4.826" size="1.778" layer="94">1</text>
<text x="-2.159" y="-4.826" size="1.778" layer="94">2</text>
<text x="3.175" y="-0.762" size="1.778" layer="94">3</text>
<text x="-4.572" y="-0.762" size="1.778" layer="94">5</text>
<text x="1.778" y="1.778" size="1.778" layer="94">6</text>
<text x="0.381" y="3.556" size="1.778" layer="94">7</text>
<rectangle x1="-1.27" y1="6.35" x2="1.27" y2="8.89" layer="94"/>
<rectangle x1="-1.27" y1="-8.255" x2="1.27" y2="-6.985" layer="94"/>
<pin name="3" x="15.24" y="0" visible="off" direction="pas" rot="R180"/>
<pin name="1" x="15.24" y="-5.08" visible="off" direction="pas" rot="R180"/>
<pin name="5" x="-15.24" y="0" visible="off" direction="pas"/>
<pin name="4" x="12.7" y="-2.54" visible="off" direction="pas" rot="R180"/>
<pin name="2" x="-15.24" y="-5.08" visible="off" direction="pas"/>
<pin name="PE1" x="12.7" y="-7.62" visible="off" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="6" x="12.7" y="2.54" visible="off" direction="pas" rot="R180"/>
<pin name="8" x="-12.7" y="2.54" visible="off" direction="pas"/>
<pin name="7" x="-15.24" y="5.08" visible="off" direction="pas"/>
<pin name="PE3" x="-12.7" y="-7.62" visible="off" length="short" direction="pwr" swaplevel="1"/>
<pin name="PE2" x="0" y="-12.7" visible="off" length="middle" direction="pwr" swaplevel="1" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="3.048" y="-6.858"/>
<vertex x="3.048" y="-5.334"/>
<vertex x="5.588" y="-5.334"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="-3.048" y="-6.858"/>
<vertex x="-3.048" y="-5.334"/>
<vertex x="-5.588" y="-5.334"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="MINI_DIN_8" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="M_DIN8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="M_DIN8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="PE1" pad="SH1"/>
<connect gate="G$1" pin="PE2" pad="SH2"/>
<connect gate="G$1" pin="PE3" pad="SH3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<modules>
<module name="TABELLE" prefix="KA" dx="30.48" dy="20.32">
<ports>
</ports>
<variantdefs>
</variantdefs>
<parts>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</module>
</modules>
<parts>
<part name="SV2" library="con-lstb" deviceset="MA04-2" device=""/>
<part name="MINI-DIN-6/8P" library="mini_din" deviceset="MINI_DIN_8" device="" value="CAT"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="114.3" y="-5.08" size="2.1844" layer="94" font="vector">DK8DE</text>
<text x="39.37" y="73.152" size="2.1844" layer="94" font="vector">1
2
3
4
5
6
7
8</text>
<text x="44.45" y="73.152" size="2.1844" layer="94" font="vector">13,8V
TX GND
GND
TX D
RX D
BAND C
RESET
TX INH</text>
<text x="40.64" y="35.56" size="2.1844" layer="94" font="vector">1
2
3
-
5
6
-
8</text>
<text x="45.72" y="35.56" size="2.1844" layer="94" font="vector">DATA IN
GND
PTT
-
DATA OUT 9600bps
DATA OUT 1200bps
-
SQL</text>
<wire x1="74.93" y1="98.552" x2="74.93" y2="95.25" width="0.1524" layer="94"/>
<wire x1="74.93" y1="95.25" x2="36.83" y2="95.25" width="0.1524" layer="94"/>
<wire x1="36.83" y1="95.25" x2="36.83" y2="92.202" width="0.1524" layer="94"/>
<wire x1="36.83" y1="92.202" x2="74.93" y2="92.202" width="0.1524" layer="94"/>
<wire x1="74.93" y1="92.202" x2="74.93" y2="88.9" width="0.1524" layer="94"/>
<wire x1="74.93" y1="88.9" x2="36.83" y2="88.9" width="0.1524" layer="94"/>
<wire x1="36.83" y1="88.9" x2="36.83" y2="85.852" width="0.1524" layer="94"/>
<wire x1="36.83" y1="85.852" x2="74.93" y2="85.852" width="0.1524" layer="94"/>
<wire x1="74.93" y1="85.852" x2="74.93" y2="82.55" width="0.1524" layer="94"/>
<wire x1="74.93" y1="82.55" x2="36.83" y2="82.55" width="0.1524" layer="94"/>
<wire x1="36.83" y1="82.55" x2="36.83" y2="79.248" width="0.1524" layer="94"/>
<wire x1="36.83" y1="79.248" x2="74.93" y2="79.248" width="0.1524" layer="94"/>
<wire x1="74.93" y1="79.248" x2="74.93" y2="75.946" width="0.1524" layer="94"/>
<wire x1="74.93" y1="75.946" x2="36.83" y2="75.946" width="0.1524" layer="94"/>
<wire x1="36.83" y1="75.946" x2="36.83" y2="72.39" width="0.1524" layer="94"/>
<wire x1="36.83" y1="72.39" x2="42.672" y2="72.39" width="0.1524" layer="94"/>
<wire x1="42.672" y1="72.39" x2="74.93" y2="72.39" width="0.1524" layer="94"/>
<wire x1="74.93" y1="72.39" x2="74.93" y2="95.25" width="0.1524" layer="94"/>
<wire x1="74.93" y1="98.552" x2="42.672" y2="98.552" width="0.1524" layer="94"/>
<wire x1="42.672" y1="98.552" x2="36.83" y2="98.552" width="0.1524" layer="94"/>
<wire x1="36.83" y1="98.552" x2="36.83" y2="75.946" width="0.1524" layer="94"/>
<wire x1="42.672" y1="98.552" x2="42.672" y2="72.39" width="0.1524" layer="94"/>
<wire x1="76.2" y1="60.96" x2="76.2" y2="57.658" width="0.1524" layer="94"/>
<wire x1="76.2" y1="57.658" x2="38.1" y2="57.658" width="0.1524" layer="94"/>
<wire x1="38.1" y1="54.61" x2="76.2" y2="54.61" width="0.1524" layer="94"/>
<wire x1="76.2" y1="54.61" x2="76.2" y2="51.308" width="0.1524" layer="94"/>
<wire x1="76.2" y1="51.308" x2="38.1" y2="51.308" width="0.1524" layer="94"/>
<wire x1="38.1" y1="48.26" x2="76.2" y2="48.26" width="0.1524" layer="94"/>
<wire x1="76.2" y1="48.26" x2="76.2" y2="44.958" width="0.1524" layer="94"/>
<wire x1="76.2" y1="44.958" x2="38.1" y2="44.958" width="0.1524" layer="94"/>
<wire x1="38.1" y1="41.656" x2="76.2" y2="41.656" width="0.1524" layer="94"/>
<wire x1="76.2" y1="41.656" x2="76.2" y2="38.354" width="0.1524" layer="94"/>
<wire x1="76.2" y1="38.354" x2="38.1" y2="38.354" width="0.1524" layer="94"/>
<wire x1="38.1" y1="34.798" x2="43.942" y2="34.798" width="0.1524" layer="94"/>
<wire x1="43.942" y1="34.798" x2="76.2" y2="34.798" width="0.1524" layer="94"/>
<wire x1="76.2" y1="34.798" x2="76.2" y2="57.658" width="0.1524" layer="94"/>
<wire x1="76.2" y1="60.96" x2="43.942" y2="60.96" width="0.1524" layer="94"/>
<wire x1="38.1" y1="60.96" x2="38.1" y2="34.798" width="0.1524" layer="94"/>
<wire x1="38.1" y1="60.96" x2="43.942" y2="60.96" width="0.1524" layer="94"/>
<wire x1="43.942" y1="60.96" x2="43.942" y2="34.798" width="0.1524" layer="94"/>
<text x="36.83" y="99.568" size="2.1844" layer="94" font="vector">CAT     MINI-DIN-8P</text>
<text x="38.1" y="61.976" size="2.1844" layer="94" font="vector">DATA    MINI-DIN-6P</text>
</plain>
<instances>
<instance part="SV2" gate="G$1" x="5.08" y="60.96" smashed="yes" rot="R270">
<attribute name="VALUE" x="-5.08" y="64.77" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="10.922" y="64.77" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="MINI-DIN-6/8P" gate="G$1" x="2.54" y="88.9" smashed="yes">
<attribute name="VALUE" x="8.89" y="99.822" size="1.778" layer="96"/>
<attribute name="NAME" x="-7.62" y="99.695" size="1.778" layer="95"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="-10.16" y="73.66" smashed="yes">
<attribute name="VALUE" x="-8.255" y="72.517" size="1.778" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-101.6" y="-25.4" smashed="yes">
<attribute name="DRAWING_NAME" x="115.57" y="-10.16" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="115.57" y="-15.24" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="128.905" y="-20.32" size="2.54" layer="94" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<wire x1="17.78" y1="88.9" x2="20.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="20.32" y1="88.9" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="40.64" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="3"/>
<wire x1="2.54" y1="40.64" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="17.78" y1="50.8" x2="17.78" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="1"/>
<wire x1="17.78" y1="50.8" x2="0" y2="50.8" width="0.1524" layer="91"/>
<wire x1="0" y1="50.8" x2="0" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="-12.7" y1="88.9" x2="-15.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="88.9" x2="-15.24" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="48.26" x2="5.08" y2="48.26" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="5"/>
<wire x1="5.08" y1="48.26" x2="5.08" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="-12.7" y1="83.82" x2="-12.7" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="71.12" x2="0" y2="71.12" width="0.1524" layer="91"/>
<wire x1="0" y1="71.12" x2="0" y2="68.58" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="15.24" y1="91.44" x2="25.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="25.4" y1="91.44" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="6"/>
<wire x1="5.08" y1="68.58" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<wire x1="5.08" y1="71.12" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="SV2" gate="G$1" pin="8"/>
<wire x1="7.62" y1="68.58" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="15.24" y1="68.58" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="15.24" y1="45.72" x2="-17.78" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="45.72" x2="-17.78" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="91.44" x2="-10.16" y2="91.44" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="15.24" y1="86.36" x2="22.86" y2="86.36" width="0.1524" layer="91"/>
<wire x1="22.86" y1="86.36" x2="22.86" y2="73.66" width="0.1524" layer="91"/>
<wire x1="22.86" y1="73.66" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="4"/>
<wire x1="2.54" y1="73.66" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="-12.7" y1="93.98" x2="-20.32" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="93.98" x2="-20.32" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="43.18" x2="7.62" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SV2" gate="G$1" pin="7"/>
<wire x1="7.62" y1="43.18" x2="7.62" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="7"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="PE3"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="-10.16" y1="81.28" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="PE2"/>
<wire x1="-10.16" y1="76.2" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
<junction x="-10.16" y="76.2"/>
<pinref part="MINI-DIN-6/8P" gate="G$1" pin="PE1"/>
<wire x1="2.54" y1="76.2" x2="15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="15.24" y1="76.2" x2="15.24" y2="81.28" width="0.1524" layer="91"/>
<junction x="2.54" y="76.2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,25.6896,66.04,SV2,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
